import time
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

def click_button(xpath, selector):
    btn_present= EC.presence_of_element_located((selector, xpath))
    WebDriverWait(driver, 10).until(btn_present)
    btn = driver.find_element(selector, xpath)
    btn.click()

def send_to_input(text, xpath, selector):
    inp_present= EC.presence_of_element_located((selector, xpath))
    WebDriverWait(driver, 10).until(inp_present)
    elem = driver.find_element(selector, xpath)
    elem.send_keys(text)
    elem.submit()

def every_downloads_chrome(driver):
    if not driver.current_url.startswith("chrome://downloads"):
        driver.get("chrome://downloads/")
    return driver.execute_script("""
        var items = document.querySelector('downloads-manager')
            .shadowRoot.getElementById('downloadsList').items;
        if (items.every(e => e.state === "COMPLETE"))
            return items.map(e => e.fileUrl || e.file_url);
        """)

driver = webdriver.Chrome('/Users/igormusalimov/PycharmProjects/Lab2_soundcloud/ChromeDriver_Driver/chromedriver')

driver.get('https://soundcloud.com');

click_button("onetrust-accept-btn-handler", By.ID)

# time.sleep(3)
# click_button(".//button[@title='Sign in']",By.XPATH)

# send_to_input("почта", ".//input[@name='email']", By.XPATH)
send_to_input("vice city", "/html/body/div[1]/div[2]/div[2]/div/div/div[2]/div/div[1]/span/span/form/input", By.XPATH)


wait = WebDriverWait(driver, 20)
titles = wait.until(EC.visibility_of_all_elements_located((By.XPATH, ".//div[@class='soundTitle__usernameTitleContainer sc-mb-0.5x']/a")))

links = [title.get_attribute('href')for title in titles]
i=0
for link in links:
    print(f'{i} {link}')
    i+=1

link_selected = False
selection = 0
time.sleep(5)
while not (link_selected == True):
    try:
        selection = 0
        time.sleep(3)
        link_selected = True
        driver.get('https://www.soundcloudme.com/');
        time.sleep(3)
        click_button('fc-button-label', By.CLASS_NAME)
        time.sleep(3)
        send_to_input(links[selection], '/html/body/div[1]/main/div/div/div/article/div/div[1]/form/div/input', By.XPATH)
        time.sleep(3)
        click_button('/html/body/div[1]/main/div/div/div/article/div/div[1]/form/button', By.XPATH)
        paths = WebDriverWait(driver, 120, 1).until(every_downloads_chrome)
    except ValueError:
        print('insert number of link')

time.sleep(3) # Let the user actually see something!

driver.quit()

